# Swagger Toolshed

This is a Chrome Extension that allows users to configure product options and drag and drop configurations to auto-populate DSS endpoints on the [Design Specification Service](https://design-specifications.design.vpsvc.com/swagger) swagger page.

## Steps
1. Clone or Download this repository
1. Navigate to the repository in a terminal
1. Run `npm install` and `npm run build`
1. Open **Chrome** and navigate to `chrome://extensions` in the URL bar
1. Turn **Developer Mode** on if it is not already
1. Select **Load Unpacked Extension**
1. Load in the `dist` folder that was generated in **Step 3**
1. Profit

### Demo:
![Swagger Toolshed Demo](./swagger-toolshed-demo.mp4)

### Additional Options

It is common to want to autofill common values across multiple requests. You can do this by selecting the **Gear Icon** at the bottom right of the extension UI. A modal will show common values to autofill into the corresponding swagger fields for every product.

