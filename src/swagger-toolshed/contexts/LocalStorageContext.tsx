/* eslint-disable no-undef */
import React, { useState, useEffect, useMemo, useContext } from "react";
import {
  StorageKeys,
  initializeLocalStorage,
  getFavoriteProducts,
  getRecentProducts,
  getAdditionalOptions,
  getAppState,
  INITIAL_APP_STATE
} from "../../syncStorageController";
import { Configuration } from "../types/Configuration";
import { AppState } from "../types/Settings";

type LocalStorage = {
  appState: AppState;
  favoriteProducts: Configuration[];
  recentProducts: Configuration[];
  additionalOptions: Record<string, string>;
}

export const LocalStorageContext = React.createContext({
  appState: INITIAL_APP_STATE,
  favoriteProducts: [],
  recentProducts: [],
  additionalOptions: {},
} as LocalStorage);

export const useLocalStorageContext = (): LocalStorage => {
  return useContext(LocalStorageContext);
};

type LocalStorageProviderProps = {
  children: React.ReactElement
};

const LocalStorageProvider: React.FC<LocalStorageProviderProps> = ({ children }) => {
  const [appState, setAppState] = useState<AppState>(INITIAL_APP_STATE);
  const [favoriteProducts, setFavoriteProducts] = useState<Configuration[]>([]);
  const [recentProducts, setRecentProducts] = useState<Configuration[]>([]);
  const [additionalOptions, setAdditionalOptions] = useState<Record<string, string>>({});
  // Initialize
  useEffect(() => {
    initializeLocalStorage();

    const hydrateAppState = () => {
      getAppState((state) => {
        setAppState(state)
      })
    }
    const hydrateRecents = () =>
      getRecentProducts((recents) => {
        setRecentProducts(recents || []);
      });
    const hydrateFavoriteProducts = () =>
      getFavoriteProducts((favorites) => {
        setFavoriteProducts(favorites || []);
      });
    const hydrateAddtionalOptions = () =>
      getAdditionalOptions((options) => {
        setAdditionalOptions(
          options || {}
        );
      });

      const onLocalStorageChange = (changes: Record<string, unknown>) => {
        if (changes[StorageKeys.MINI_PICC_APP_STATE]) {
          hydrateAppState();
        }
        if (changes[StorageKeys.MINI_PICC_RECENTS]) {
          hydrateRecents();
        }
        if (changes[StorageKeys.MINI_PICC_FAVORITES]) {
          hydrateFavoriteProducts();
        }
        if (changes[StorageKeys.MINI_PICC_ADDITIONAL_OPTIONS]) {
          hydrateAddtionalOptions();
        }
      }
    chrome.storage.onChanged.addListener(onLocalStorageChange);
    // Hyrdate state on mount
    hydrateAppState();
    hydrateFavoriteProducts();
    hydrateRecents();
    hydrateAddtionalOptions();

    return () => chrome.storage.onChanged.removeListener(onLocalStorageChange);
  }, []);

  const contextObject = useMemo(
    () => ({
      appState,
      favoriteProducts,
      recentProducts,
      additionalOptions,
    }),
    [appState, favoriteProducts, recentProducts, additionalOptions]
  );

  return (
    <LocalStorageContext.Provider value={contextObject}>
      {children}
    </LocalStorageContext.Provider>
  );
}

export default LocalStorageProvider;