import React from 'react';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Configuration } from '../../types/Configuration';
import SavedAndRecentListItem from './SavedAndRecentListItem';

const useStyles = makeStyles((theme) => ({
  noContentDiv: {
    width: '100%',
    textAlign: 'center',
    padding: '10px',
  },
  listItem: {
    cursor: 'pointer',
  },
}));

interface ShortcutListProps {
  listItems: Configuration[];
  listItemText: string;
  infoText: string;
  listItemIcon: React.ReactElement;
  isExpanded: boolean;
  onExpandList: () => void;
  onRemove: (configId: string) => void;
  onEdit: (config: Configuration) => void;
  additionalActionIcon?: React.ReactElement;
  onAdditionalActionClick?: (arg: any) => void;
}

export const ShortcutList = ({
  listItems,
  listItemText,
  infoText,
  listItemIcon,
  isExpanded,
  onExpandList,
  onRemove,
  onEdit,
  additionalActionIcon,
  onAdditionalActionClick,
}: ShortcutListProps) => {
  const classes = useStyles();

  return (
    <>
      <ListItem
        key={`list-item-favorites`}
        className={classes.listItem}
        onClick={onExpandList}
        divider
      >
        <ListItemIcon>{listItemIcon}</ListItemIcon>
        <ListItemText primary={listItemText} />
        {additionalActionIcon && onAdditionalActionClick ? (
          <IconButton size='small' onClick={onAdditionalActionClick}>
            {additionalActionIcon}
          </IconButton>
        ) : null}
      </ListItem>
      <Collapse in={isExpanded} key='collapsible-favorites'>
        {listItems.length ? (
          listItems.map((listItem) => {
            return (
              <SavedAndRecentListItem
                config={listItem}
                onRemove={onRemove}
                onEdit={onEdit}
              />
            );
          })
        ) : (
          <div className={classes.noContentDiv}>{infoText}</div>
        )}
      </Collapse>
    </>
  );
};
