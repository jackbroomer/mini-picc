/* eslint-disable no-undef */
import { useState, useEffect } from 'react';
import {
  IconButton,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  TextField,
} from '@material-ui/core';
import { Favorite, FavoriteBorder } from '@material-ui/icons';
import isEqual from 'lodash/isEqual';
import { activateOnEnter } from '../../utils';
import {
  addFavoriteProduct,
  removeFavoriteProduct,
} from '../../../syncStorageController';
import { useLocalStorageContext } from '../../contexts/LocalStorageContext';
import { Configuration } from '../../types/Configuration';

type FavoriteButtonProps = {
  config: Configuration;
};

const FavoriteButton: React.FC<FavoriteButtonProps> = ({ config }) => {
  const { favoriteProducts } = useLocalStorageContext();
  const [isOpen, setIsOpen] = useState(false);
  const [isFavorited, setIsFavorited] = useState(false);
  const [configName, setConfigName] = useState('');

  useEffect(() => {
    if (
      favoriteProducts.find((favoritedProduct) =>
        isEqual(favoritedProduct.config_uuid, config.config_uuid)
      )
    ) {
      setIsFavorited(true);
      return;
    }
    setIsFavorited(false);
  }, [favoriteProducts, config]);

  const onSave = () => {
    addFavoriteProduct({
      ...config,
      config_name: configName,
    });
    setIsOpen(false);
    setConfigName('');
  };

  return (
    <>
      {isFavorited ? (
        <IconButton
          title='Remove Favorite'
          onClick={() => removeFavoriteProduct(config.config_uuid)}
        >
          <Favorite />
        </IconButton>
      ) : (
        <IconButton title='Add Favorite' onClick={() => setIsOpen(true)}>
          <FavoriteBorder />
        </IconButton>
      )}
      <Dialog fullWidth open={isOpen}>
        <DialogContent>
          <TextField
            autoFocus
            variant='outlined'
            fullWidth
            label='Give your configuration a name'
            value={configName}
            onChange={(e) => setConfigName(e.target.value)}
            onKeyUp={(e) =>
              activateOnEnter(e as unknown as KeyboardEvent, onSave)
            }
          />
          <DialogActions>
            <Button
              onClick={() => setIsOpen(false)}
              variant='contained'
              color='secondary'
            >
              Cancel
            </Button>
            <Button onClick={onSave} variant='contained' color='primary'>
              Save
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </>
  );
};
export default FavoriteButton;
