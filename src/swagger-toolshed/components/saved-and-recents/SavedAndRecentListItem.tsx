import { useMemo, useState, useEffect, useRef, DragEvent } from 'react';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Edit, Delete, MoreHoriz, OpenInNew } from '@material-ui/icons';
import { useLocalStorageContext } from '../../contexts/LocalStorageContext';
import Cab from '../collapsible-action-buttons/Cab';
import StudioLinkPopper from '../../extensions/mini-picc/components/bottom-button-dock/StudioLinkPopper';
import { Configuration } from '../../types/Configuration';

const useStyles = makeStyles({
  actionButtons: {
    marginRight: '10px',
    display: 'flex',
    '&.dragging': {
      display: 'none',
    },
  },
  listItem: {
    width: '100vw',
    flexDirection: 'column',
  },
  listItemText: {
    display: 'flex',
    width: '100%',
  },
  moreButton: {
    right: '10px',
  },
  accordianDetails: {
    display: 'block',
    padding: 0,
  },
  accordianSummaryContent: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    '& p': {
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },
  },
});

type SavedAndRecentListItemProps = {
  config: Configuration;
  onRemove: (configId: string) => void;
  onEdit: (config: Configuration) => void;
};

const SavedAndRecentListItem: React.FC<SavedAndRecentListItemProps> = ({
  config,
  onRemove,
  onEdit,
}) => {
  const classes = useStyles();
  const [isDragging, setIsDragging] = useState<boolean>(false);
  const [isCabOpen, setIsCabOpen] = useState<boolean>(false);
  const [isStudioPopperOpen, setIsStudioPopperOpen] = useState<boolean>(false);
  const [canOpenInStudio, setCanOpenInStudio] = useState<boolean>(false);
  const { additionalOptions } = useLocalStorageContext();
  const accordionRef =
    useRef<HTMLElement>() as React.MutableRefObject<HTMLElement>;

  useEffect(() => {
    if (config.productKey && config.optionSelections) {
      setCanOpenInStudio(true);
      return;
    }
    setCanOpenInStudio(false);
  }, [config]);

  const setData = (event: DragEvent) => {
    setIsCabOpen(false);
    setIsDragging(true);
    event.dataTransfer.setData(
      'swaggerToolshed',
      JSON.stringify({ ...additionalOptions, ...config })
    );
    event.dataTransfer.dropEffect = 'copy';
  };

  const listItemActions = useMemo(
    () => [
      {
        title: 'Edit',
        icon: <Edit />,
        run: () => onEdit(config),
      },
      {
        title: 'Delete',
        icon: <Delete />,
        run: () => onRemove(config.config_uuid),
      },
      ...(canOpenInStudio
        ? [
            {
              title: 'Open in Studio',
              icon: <OpenInNew />,
              run: () => setIsStudioPopperOpen((open) => !open),
            },
          ]
        : []),
    ],
    [onRemove, config, onEdit, canOpenInStudio]
  );

  const getListName = () => {
    if (canOpenInStudio && !config.config_name) {
      return `(${config.productKey}) ${config.productName}`;
    }
    return config.config_name || 'Untitled';
  };

  const hoverText = useMemo(() => {
    const configClone = { ...config } as Partial<Configuration>;

    delete configClone.config_name;
    delete configClone.config_uuid;
  }, [config])

  return (
    <>
      <Accordion
        expanded={isCabOpen}
        onClick={() => setIsCabOpen((open) => !open)}
        square
      >
        <AccordionSummary
          expandIcon={!isDragging && <MoreHoriz />}
          classes={{
            content: classes.accordianSummaryContent,
          }}
          draggable='true'
          onDragStart={setData}
          onDragEnd={() => setIsDragging(false)}
          title={JSON.stringify(
            hoverText,
            null,
            2
          )}
        >
          <Typography>{getListName()}</Typography>
        </AccordionSummary>
        <AccordionDetails
          ref={accordionRef}
          classes={{ root: classes.accordianDetails }}
        >
          <Cab actions={listItemActions} />
        </AccordionDetails>
      </Accordion>
      {canOpenInStudio && (
        <StudioLinkPopper
          isOpen={isStudioPopperOpen}
          close={() => setIsStudioPopperOpen(false)}
          productConfig={config}
          ref={accordionRef}
        />
      )}
    </>
  );
};
export default SavedAndRecentListItem;
