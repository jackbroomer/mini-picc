import React, { useState, useMemo, MouseEvent, useCallback } from 'react';
import { List, Typography } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { Favorite, Schedule, AddBox } from '@material-ui/icons';
import { ShortcutList } from './ShortcutList';
import {
  removeRecentProduct,
  removeFavoriteProduct,
  addFavoriteProduct,
  setAppState,
} from '../../../syncStorageController';
import CustomConfiguration from '../CustomConfiguration';
import { useLocalStorageContext } from '../../contexts/LocalStorageContext';
import { Configuration, CustomOption } from '../../types/Configuration';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  header: {
    padding: '10px',
  },
  noContentDiv: {
    width: '100%',
    textAlign: 'center',
    padding: '10px',
  },
  listItem: {
    cursor: 'pointer',
  },
}));

const INITIAL_CONFIG = {} as Configuration;

const SavedAndRecents: React.FC = () => {
  const classes = useStyles();
  const { appState, favoriteProducts, recentProducts } =
    useLocalStorageContext();
  const [customConfigOpen, setCustomConfigOpen] = useState<boolean>(false);
  const [currentConfiguration, setCurrentConfiguration] =
    useState<Configuration>(INITIAL_CONFIG);

  const onAddCustomConfig = (e: MouseEvent) => {
    e.stopPropagation();
    setCustomConfigOpen(true);
  };

  const onEdit = (config: Configuration) => {
    setCurrentConfiguration(config);
    setCustomConfigOpen(true);
  };

  const onCustomConfigClose = () => {
    setCurrentConfiguration(INITIAL_CONFIG);
    setCustomConfigOpen(false);
  };

  const onCustomConfigSave = (customOptions: CustomOption[]) => {
    const productConfig = customOptions.reduce((acc, currOption) => {
      const { key, value } = currOption;
      if (!key) return acc;
      acc[key] = value;
      return acc;
    }, {} as Configuration);
    addFavoriteProduct(productConfig);
  };

  const openCloseFavorites = useCallback(() => {
    setAppState({ ...appState, isFavoritesOpen: !appState?.isFavoritesOpen });
  }, [appState]);

  const openCloseRecents = useCallback(() => {
    setAppState({ ...appState, isRecentsOpen: !appState?.isRecentsOpen });
  }, [appState]);

  /**
   * Should refactor the favorites and recent list into reusable list component.
   * Each component can be in charge of own open and close state
   */
  const favoritesList = useMemo(
    () => (
      <ShortcutList
        listItems={favoriteProducts}
        listItemText='Favorites'
        infoText='Try saving your frequently used product configurations!'
        listItemIcon={<Favorite />}
        isExpanded={appState.isFavoritesOpen}
        onExpandList={openCloseFavorites}
        onRemove={removeFavoriteProduct}
        onEdit={onEdit}
        additionalActionIcon={<AddBox />}
        onAdditionalActionClick={onAddCustomConfig}
      />
    ),
    [appState?.isFavoritesOpen, favoriteProducts, openCloseFavorites]
  );

  const recentsList = useMemo(
    () => (
      <ShortcutList
        listItems={recentProducts}
        listItemText='Recents'
        infoText='Your recently used products will show here!'
        listItemIcon={<Schedule />}
        isExpanded={appState.isRecentsOpen}
        onExpandList={openCloseRecents}
        onRemove={removeRecentProduct}
        onEdit={onEdit}
      />
    ),
    [appState?.isRecentsOpen, recentProducts, openCloseRecents]
  );

  return (
    <>
      <Typography
        component='nav'
        className={classes.header}
        color='primary'
        variant='h5'
      >
        {'Drag & Drop Shortcuts'}
      </Typography>
      <List
        component='nav'
        aria-labelledby='nested-list-subheader'
        className={classes.root}
      >
        {favoritesList}
        {recentsList}
      </List>
      <CustomConfiguration
        title='Edit Configuration'
        currentConfiguration={currentConfiguration}
        isOpen={customConfigOpen}
        onClose={onCustomConfigClose}
        onSave={onCustomConfigSave}
      />
    </>
  );
};
export default SavedAndRecents;
