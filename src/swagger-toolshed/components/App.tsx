/* eslint-disable no-undef */
import React, { useState, Suspense } from 'react';
import { AppBar, Typography, Fab } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Settings } from '@material-ui/icons';
import AutocompleteSearch from '../extensions/mini-picc/components/AutocompleteSearch';
import SavedAndRecents from './saved-and-recents/SavedAndRecents';
import { SelectedProductConfiguration } from '../types/Configuration';

const ConfigureProduct = React.lazy(
  () => import('../extensions/mini-picc/components/configure-product-options/ConfigureProduct')
);
const CommonConfiguration = React.lazy(() => import('./CommonConfiguration'));

const useStyles = makeStyles({
  container: {
    width: '400px',
    minHeight: '750px',
    position: 'relative',
    marginTop: '40px',
  },
  autocompleteContainer: {
    display: 'flex',
  },
  appBar: {
    textAlign: 'center',
  },
  backButton: {
    position: 'absolute',
    margin: '20px 5px',
  },
  settingsButton: {
    position: 'fixed',
    bottom: '10px',
    right: '10px',
  },
});

const App: React.FC = () => {
  const classes = useStyles();
  const [selectedProduct, setSelectedProduct] =
    useState<SelectedProductConfiguration | null>(null);
  const [additionalOptionsOpen, setAdditionalOptionsOpen] =
    useState<boolean>(false);

  return (
    <div className={classes.container}>
      <AppBar className={classes.appBar}>
        <Typography variant='h4'>Swagger Toolshed</Typography>
      </AppBar>
      <AutocompleteSearch
        selectedProduct={selectedProduct}
        setSelectedProduct={setSelectedProduct}
      />
      <Suspense fallback={<div>Loading...</div>}>
        <>
        {selectedProduct && <ConfigureProduct product={selectedProduct} />}
        <CommonConfiguration
          isOpen={additionalOptionsOpen}
          onClose={() => setAdditionalOptionsOpen(false)}
        />
        </>
      </Suspense>
      {!selectedProduct && (
        <>
          <SavedAndRecents />
          <Fab
            color='primary'
            onClick={() => setAdditionalOptionsOpen(true)}
            className={classes.settingsButton}
          >
            <Settings />
          </Fab>
        </>
      )}
    </div>
  );
};

export default App;
