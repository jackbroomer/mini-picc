import { makeStyles } from '@material-ui/core';
import { RingLoader } from 'react-spinners';

const useStyles = makeStyles({
  spinningLoader: {
    position: 'absolute',
    height: 'fit-content',
    width: 'fit-content',
    top: '30%',
    left: '40%',
  },
});

const RingSpinner: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.spinningLoader}>
      <RingLoader />
    </div>
  );
};
export default RingSpinner;
