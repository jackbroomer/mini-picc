import { useMemo } from 'react';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import makeStyles from '@material-ui/core/styles/makeStyles';
import RigidButton from '../RigidButton';

const useStyles = makeStyles({
  collapse: {
    width: '100vw',
    paddingRight: '30px',
  },
  rigidButton: {
    borderRadius: 0,
  },
});

const StyledRigidButton = (props: any) => {
  const classes = useStyles();
  return <RigidButton className={classes.rigidButton} {...props} />;
};

type CabAction = {
  title: string;
  icon: JSX.Element;
  run: () => void;
};

type CabProps = {
  actions: CabAction[];
};

const Cab: React.FC<CabProps> = ({ actions }) => {
  const actionButtons = useMemo(
    () =>
      actions.map((action) => (
        <StyledRigidButton
          title={action.title}
          onClick={(e: any) => {
            e.stopPropagation();
            action.run();
          }}
        >
          {action.icon}
        </StyledRigidButton>
      )),
    [actions]
  );

  return (
    <ButtonGroup
      disableRipple
      disableElevation
      fullWidth
      color='default'
      variant='contained'
    >
      {actionButtons}
    </ButtonGroup>
  );
};
export default Cab;
