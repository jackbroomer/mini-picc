import CustomConfiguration from './CustomConfiguration';
import { useLocalStorageContext } from '../contexts/LocalStorageContext';
import { setAdditionalOptions } from '../../syncStorageController';
import { Configuration, CustomOption } from '../types/Configuration';

type CommonConfigurationProps = {
  isOpen: boolean;
  onClose: () => void;
};

const CommonConfiguration: React.FC<CommonConfigurationProps> = ({
  isOpen,
  onClose,
}) => {
  const { additionalOptions } = useLocalStorageContext();

  const onSave = (currentOptions: CustomOption[]) => {
    setAdditionalOptions({
      ...currentOptions.reduce((acc, currentOption) => {
        if (!currentOption.key || !currentOption.value) {
          return acc;
        }
        acc[currentOption.key] = currentOption.value;
        return acc;
      }, {} as Configuration),
    } as Record<string, string>);
    onClose();
  };

  return (
    <CustomConfiguration
      title='Additional Options'
      showConfigurationName={false}
      isOpen={isOpen}
      onClose={onClose}
      onSave={onSave}
      currentConfiguration={additionalOptions}
    />
  );
};
export default CommonConfiguration;
