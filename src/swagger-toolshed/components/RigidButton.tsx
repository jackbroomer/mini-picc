import React, { DragEventHandler, forwardRef, RefObject } from 'react';
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  rigidButton: {
    borderRadius: 0,
  },
});

// Figure this out
type RigidButtonProps = any;

const RigidButton = forwardRef<HTMLElement, RigidButtonProps>(({ children, className, draggable, color, onDragStart, ...buttonProps }, ref) => {
  const classes = useStyles();
  return (
    <Button
      ref={ref as RefObject<HTMLButtonElement>}
      className={`${classes.rigidButton} ${className}`}
      variant="contained"
      fullWidth
      draggable={!!draggable}
      color={color || "default"}
      onDragStart={onDragStart as unknown as DragEventHandler}
      {...buttonProps}
    >
      {children}
    </Button>
  );
});

export default RigidButton;
