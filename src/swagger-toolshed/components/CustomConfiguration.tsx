import React, { useState, useEffect, useMemo, useCallback } from 'react';
import { v4 as uuidv4 } from 'uuid';
import {
  Dialog,
  DialogTitle,
  DialogActions,
  IconButton,
  Button,
  List,
  ListItem,
  TextField,
  DialogContent,
  Slide,
  makeStyles,
  TextFieldProps,
} from '@material-ui/core';
import { RemoveCircle } from '@material-ui/icons';
import { Configuration, CustomOption } from '../types/Configuration';

const useStyles = makeStyles({
  textFieldRoot: {
    margin: '0px 4px',
  },
  addButton: {
    float: 'right',
  },
});

const ListTextField = (props: any) => {
  const classes = useStyles();
  return (
    <TextField
      classes={{
        root: classes.textFieldRoot,
      }}
      variant='outlined'
      size='small'
      {...props}
    />
  );
};

type CustomConfigurationProps = {
  title: string;
  isOpen: boolean;
  onSave: (customOptions: CustomOption[]) => void;
  onClose: () => void;
  currentConfiguration: Configuration | Record<string, string>;
  showConfigurationName?: boolean;
};

/**
 * A reusable component for generating lists to create and edit custom configurations.
 * Renders a list of input text fields side by side, the left field is the configuration
 * option name and the right side is the value.
 */
const CustomConfiguration: React.FC<CustomConfigurationProps> = ({
  title,
  isOpen,
  onSave,
  onClose,
  currentConfiguration,
  showConfigurationName = true, // Show/hide the configuration name text field
}) => {
  const classes = useStyles();
  const [loadedCurrentConfig, setLoadedCurrentConfig] =
    useState<boolean>(false);
  const [configurationName, setConfigurationName] = useState<string>('');
  const [customOptions, setCustomOptions] = useState<CustomOption[]>([]);

  // Load in the current configuration
  useEffect(() => {
    if (!isOpen) {
      return;
    }
    if (!currentConfiguration || !Object.keys(currentConfiguration).length) {
      setLoadedCurrentConfig(true);
      return;
    }
    try {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { config_uuid, config_name, ...restOfConfiguration } =
        currentConfiguration;
      setConfigurationName(config_name); // Set the name

      // Parse the passed in configuration to key/value pairs
      setCustomOptions(
        Object.keys(restOfConfiguration)
          .sort() // Sort alphabetically
          .reduce((acc, currOptionKey) => {
            const value = restOfConfiguration[currOptionKey];
            return [
              ...acc,
              {
                key: currOptionKey,
                value:
                  typeof value === 'object' ? JSON.stringify(value) : value,
                in: true,
              },
            ];
          }, [] as CustomOption[])
      );
    } catch (e) {
      console.log(e);
    } finally {
      setLoadedCurrentConfig(true);
    }
  }, [currentConfiguration, isOpen]);

  // Make sure there is always at least one visible field
  useEffect(() => {
    if (isOpen && loadedCurrentConfig && !customOptions.length) {
      setCustomOptions([
        {
          key: '',
          value: '',
          in: true,
        },
      ]);
    }
  }, [isOpen, customOptions, loadedCurrentConfig]);

  // Add new blank option
  const addNewOption = useCallback(() => {
    setCustomOptions((options) => [
      ...options,
      { key: '', value: '', in: true },
    ]);
  }, []);

  // Tirggers list item component unmount
  const onHideOption = useCallback((optionIndex) => {
    setCustomOptions((options) =>
      options.map((currOption, index) => {
        if (optionIndex === index) {
          return {
            ...currOption,
            in: false,
          };
        }
        return currOption;
      })
    );
  }, []);

  // Called when a list item is unmounted
  const onRemoveOption = useCallback((optionIndex) => {
    setCustomOptions((options) =>
      options.filter((currOption, index) => optionIndex !== index)
    );
  }, []);

  const onOptionChange = useCallback((optionIndex, optionChange) => {
    const { key, value } = optionChange;
    setCustomOptions((options) =>
      options.map((currOption, index) => {
        if (index === optionIndex) {
          if (key !== undefined) {
            return {
              ...currOption,
              key,
            };
          } else if (value !== undefined) {
            return {
              ...currOption,
              value,
            };
          }
        }
        return currOption;
      })
    );
  }, []);

  const resetValues = () => {
    setLoadedCurrentConfig(false);
    setConfigurationName('');
    setCustomOptions([]);
  };

  const closeModal = () => {
    resetValues();
    onClose();
  };

  const handleOnSave = () => {
    onSave([
      {
        key: 'config_uuid',
        value: currentConfiguration.config_uuid || uuidv4(),
      },
      ...customOptions,
      ...(showConfigurationName
        ? [
            {
              key: 'config_name',
              value: configurationName || 'Untitled Configuration',
            },
          ]
        : []),
    ]);
    closeModal();
  };

  const customOptionsList = useMemo(
    () =>
      customOptions.map((option, index) => (
        <Slide
          in={option.in}
          onExited={() => onRemoveOption(index)}
          direction='left'
        >
          <ListItem disableGutters divider>
            <ListTextField
              label='Key'
              value={option.key}
              onChange={(e: any) => onOptionChange(index, { key: e.target.value })}
            />
            <ListTextField
              label='Value'
              value={option.value}
              onChange={(e: any) => onOptionChange(index, { value: e.target.value })}
            />
            <IconButton size='small' onClick={() => onHideOption(index)}>
              <RemoveCircle />
            </IconButton>
          </ListItem>
        </Slide>
      )),
    [customOptions, onRemoveOption, onOptionChange, onHideOption]
  );

  return (
    <Dialog open={isOpen} fullScreen>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        {showConfigurationName && (
          <TextField
            fullWidth
            variant='outlined'
            size='small'
            label='Configuration Name'
            placeholder='Name your configuration'
            onChange={(e) => setConfigurationName(e.target.value)}
            value={configurationName}
          />
        )}
        <List>{customOptionsList}</List>
        <Button
          className={classes.addButton}
          color='primary'
          size='small'
          onClick={addNewOption}
        >
          Add Value
        </Button>
      </DialogContent>
      <DialogActions>
        <Button variant='contained' color='secondary' onClick={closeModal}>
          Cancel
        </Button>
        <Button variant='contained' color='primary' onClick={handleOnSave}>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default CustomConfiguration;
