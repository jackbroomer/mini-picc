export const activateOnEnter = (e: KeyboardEvent, callback: () => void): void => {
  if (e.code === "Enter") {
    callback();
  }
}