export type Configuration = {
  config_uuid: string;
  config_name: string;
  [index: string]: string | Record<string, string | number> | number;
}

export type SelectedProductConfiguration = {
  name: string;
  productKey: string;
  version: number;
}

type CustomOption = {
  key: string;
  value: string | number;
  in?: boolean;
}