import { SelectedProductConfiguration } from "./Configuration";
import { SelectedOption } from "../extensions/mini-picc/components/configure-product-options/types";

type AppState = {
  isFavoritesOpen: boolean;
  isRecentsOpen: boolean;
  lastSelectedProduct: SelectedProductConfiguration | null;
  lastSelectedOptions: SelectedOption[] | null;
}