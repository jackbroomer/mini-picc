import axios from 'axios';
import qs from 'qs';
import { Configuration } from '../../../types/Configuration';

export const validateStudioUrl = async (
  productConfig: Configuration,
  market: string
): Promise<{ passed: boolean }> => {
  const { optionSelections, productKey } = productConfig;
  const queryParams = qs.stringify({
    productKey,
    optionSelections,
    market,
    requester: 'swagger',
  });
  const validUrl = `https://studio-product-validation.design.vpsvc.com/product?${queryParams}`;
  return (await axios.get(validUrl)).data;
};
