import qs from 'qs';
import { Configuration } from '../../../types/Configuration';

const requestor = 'swagger';

export const getProductSurface = (productConfig: Configuration): string => {
  const { productKey, productVersion, optionSelections } = productConfig;
  const qsParams = qs.stringify({
    optionSelections,
    requestor,
  });
  return `https://surfaces.products.vpsvc.com/surfaces/${productKey}/${productVersion}?${qsParams}`;
};

export const getProductSurfaceSvg = (productConfig: Configuration): string => {
  const { productKey, productVersion, optionSelections } = productConfig;
  const qsParams = qs.stringify({
    optionSelections,
    requestor,
  });
  return `https://surfaces.products.vpsvc.com/surfaces/${productKey}/${productVersion}/svg?${qsParams}`;
};
