import axios from 'axios';
import qs from 'qs';
import {
  getProductCatalogCache,
  setProductCatalogCache,
} from '../../../../syncStorageController';

export type ProductCatalogConfig = {
  name: string;
  productKey: string;
  version: number;
};

export type ProductOptions = {
  name: string;
  values: Array<string | number>;
};

export type CompatibleProductOptions = {
  list: ProductOptions[];
};

const allProductsUrl =
  'https://catalog-transition.products.vpsvc.com/api/v2/products/current?requestor=swagger-toolshed';
export const getAllProducts = async (): Promise<ProductCatalogConfig[]> =>
  await new Promise(async (res) => {
    let productCatalog: ProductCatalogConfig[] = [];

    await new Promise((resolve) => {
      getProductCatalogCache((data) => {
        if (!data?.ttl || Date.now() >= data.ttl) {
          resolve([]);
          return;
        }
        productCatalog = data.productCatalog;
        resolve(productCatalog);
      });
    });

    if (productCatalog.length === 0) {
      productCatalog = (await axios.get(allProductsUrl)).data;
      setProductCatalogCache(productCatalog);
    }

    res(productCatalog);
  });

export const getProductOptions = async (
  productKey: string,
  version: number,
  selectedOptions?: Record<string, string>
): Promise<CompatibleProductOptions> => {
  let productOptionsUrl = `https://catalog.products.vpsvc.com/api/v2/products/${productKey}/getCompatibleOptions?requestor=swagger`;
  if (selectedOptions) {
    const queryParams = qs.stringify({
      selectedOptions,
    });
    productOptionsUrl = `https://catalog.products.vpsvc.com/api/v2/products/${productKey}/${version}/getCompatibleOptions?${queryParams}&requestor=swagger`;
  }
  return (await axios(productOptionsUrl)).data;
};
