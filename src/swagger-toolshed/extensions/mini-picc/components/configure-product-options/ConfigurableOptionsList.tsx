import React, { useState, useEffect, useMemo, useCallback } from 'react';
import {
  List,
  ListSubheader,
  ListItem,
  Collapse,
  Divider,
  ListItemText,
  ListItemIcon,
  Typography,
  IconButton,
} from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import { ArrowDropDown, ArrowRight, MenuOpen, Menu } from '@material-ui/icons';
import {
  getProductOptions,
  ProductOptions,
} from '../../clients/productCatalogClient';
import { SelectedProductConfiguration } from '../../../../types/Configuration';
import { createSelectedConfigurableOptions } from './utils';
import { SelectedOption } from "./types"

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    paddingBottom: '60px',
  },
  listSubheader: {
    display: 'inline-flex',
    height: '33px',
    width: '100%',
  },
  listItem: {
    height: '35px',
  },
  collapse: {
    width: 'fit-content',
    padding: '0px 15px',
  },
  toggleButtonGroup: {
    padding: '10px',
    overflowX: 'auto',
    maxWidth: '90vw',
  },
  toggleButton: {
    height: '25px',
    whiteSpace: 'nowrap',
  },
  iconButton: {
    marginLeft: 'auto',
  },
}));

type ConfigurableOptionsListProps = {
  product: SelectedProductConfiguration;
  configurableOptions: ProductOptions[];
  selectedOptions: SelectedOption[];
  changeSelectedOption: (
    selectedOption: SelectedOption,
    value: string | number
  ) => void;
};

const ConfigurableOptionsList: React.FC<ConfigurableOptionsListProps> = ({
  product,
  configurableOptions,
  selectedOptions,
  changeSelectedOption,
}) => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState<boolean[]>([]);
  const [guided, setGuided] = useState<boolean>(true);
  const [optionCompatability, setOptionCompatability] =
    useState<ProductOptions[]>(configurableOptions);

  /**
   * Not ALL options are compatible with each other. As a user selects
   * options, we need to check which are still available and disable the
   * ones that are not. This ensures the product studio link will load
   */
  useEffect(() => {
    const getNewProductOptions = async () => {
      const newOptionsList = (
        await getProductOptions(
          product.productKey,
          product.version,
          createSelectedConfigurableOptions(
            configurableOptions,
            selectedOptions
          )
        )
      ).list;
      setOptionCompatability(newOptionsList);
    };
    getNewProductOptions();
  }, [configurableOptions, selectedOptions, product]);

  // Open only the first selected option
  useEffect(() => {
    setIsOpen(selectedOptions.map((value, index) => index === 0));
  }, [product.productKey]);

  const onOptionSelection = useCallback(
    (index, value) => {
      changeSelectedOption(index, value);
      if (guided) {
        setIsOpen((openArray) => {
          openArray[index] = false;
          if (index + 1 < openArray.length) {
            openArray[index + 1] = true;
          }
          return [...openArray];
        });
      }
    },
    [guided, changeSelectedOption]
  );

  const onListItemClick = useCallback((index) => {
    setIsOpen((openArray) => {
      openArray[index] = !openArray[index];
      return [...openArray];
    });
    setGuided(false);
  }, []);

  const renderConfigurableOptions = useMemo(
    () =>
      configurableOptions.map((option, index) => (
        <>
          <ListItem
            key={`list-item-${option.name}`}
            className={classes.listItem}
            button
            onClick={() => onListItemClick(index)}
          >
            <ListItemIcon>
              {isOpen[index] ? <ArrowDropDown /> : <ArrowRight />}
            </ListItemIcon>
            <ListItemText primary={option.name} />
          </ListItem>
          <Collapse
            className={classes.collapse}
            in={isOpen[index]}
            key={`collapsible-${option.name}`}
          >
            <ToggleButtonGroup
              className={classes.toggleButtonGroup}
              exclusive
              value={selectedOptions[index]}
              onChange={(
                _e: React.MouseEvent<HTMLElement, MouseEvent>,
                value: string
              ) => {
                onOptionSelection(index, value);
              }}
            >
              {option.values.map((value) => (
                <ToggleButton
                  disabled={!optionCompatability[index].values.includes(value)}
                  className={classes.toggleButton}
                  value={value}
                  key={`toggle-button-${value}`}
                >
                  {value}
                </ToggleButton>
              ))}
            </ToggleButtonGroup>
          </Collapse>
          <Divider />
        </>
      )),
    [
      configurableOptions,
      isOpen,
      selectedOptions,
      onOptionSelection,
      onListItemClick,
      optionCompatability,
    ]
  );

  const toggleAll = useCallback((toggleOpen) => {
    setIsOpen((openArray) => new Array(openArray.length).fill(toggleOpen));
  }, []);

  const iconButton = useMemo(() => {
    const openOptionsExist = isOpen.find((optionIsOpen) => optionIsOpen);
    if (openOptionsExist) {
      return (
        <IconButton
          onClick={() => toggleAll(false)}
          title='Collapse All'
          className={classes.iconButton}
        >
          <MenuOpen />
        </IconButton>
      );
    }
    return (
      <IconButton
        onClick={() => toggleAll(true)}
        title='Expand All'
        className={classes.iconButton}
      >
        <Menu />
      </IconButton>
    );
  }, [isOpen, toggleAll]);

  return (
    <List
      component='nav'
      aria-labelledby='nested-list-subheader'
      subheader={
        <ListSubheader
          className={classes.listSubheader}
          color='primary'
          component='li'
          id='nested-list-subheader'
        >
          <Typography variant='h6'>Select Product Attributes</Typography>
          {iconButton}
        </ListSubheader>
      }
      className={classes.root}
    >
      {renderConfigurableOptions}
    </List>
  );
};
export default ConfigurableOptionsList;
