import React, { useState, useEffect, useCallback } from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { v4 as uuidv4 } from 'uuid';
import FavoriteButton from '../../../../components/saved-and-recents/FavoriteButton';
import ButtonDock from '../bottom-button-dock/ButtonDock';
import RingSpinner from '../../../../components/loaders/RingSpinner';
import {
  getProductOptions,
  ProductOptions,
} from '../../clients/productCatalogClient';
import ConfigurableOptionsList from './ConfigurableOptionsList';
import {
  SelectedProductConfiguration,
  Configuration,
} from '../../../../types/Configuration';
import { createSelectedConfigurableOptions } from './utils';
import { SelectedOption } from './types';

const useStyles = makeStyles({
  headerInfo: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '0px 16px 10px 16px',
  }
});

type ConfigureProductsProps = {
  product: SelectedProductConfiguration;
};

const ConfigureProduct: React.FC<ConfigureProductsProps> = ({ product }) => {
  const classes = useStyles();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [configurableOptions, setConfigurableOptions] = useState<
    ProductOptions[]
  >([]);
  const [selectedOptions, setSelectedOptions] = useState<SelectedOption[]>([]);
  const [currentProductConfig, setCurrentProductConfig] =
    useState<Configuration>({} as Configuration);

  // Retrieve all compatiable options for the current product
  useEffect(() => {
    getProductOptions(product.productKey, product.version).then((response) => {
      const { list: compatibleOptionsList } = response;
      setConfigurableOptions(compatibleOptionsList);
      // If there is only one configurable option, select it
      setSelectedOptions(
        compatibleOptionsList.map((option) => {
          if (option.values.length === 1) {
            return option.values[0];
          }
          return null;
        })
      );
      setIsLoading(false);
    });
  }, [product.productKey, product.version]);

  const changeSelectedOption = useCallback(
    (optionIndex, value) => {
      // If there is only one configurable option, don't allow the user to unselect it
      if (configurableOptions[optionIndex].values.length === 1) {
        return;
      }
      if (value) {
        setSelectedOptions((a) => {
          // Deselect if the current option is the same as the value
          if (a[optionIndex] === value) {
            a[optionIndex] = null;
          } else {
            a[optionIndex] = value;
          }
          return [...a];
        });
      }
    },
    [configurableOptions]
  );

  // Update the current product config when the selectedOption, product, or configurable options change
  useEffect(() => {
    setCurrentProductConfig((config) => ({
      config_uuid: config?.config_uuid || uuidv4(),
      config_name: "",
      productName: product.name,
      productKey: product.productKey,
      productVersion: `${product.version}`,
      optionSelections: createSelectedConfigurableOptions(
        configurableOptions,
        selectedOptions
      ),
    }));
  }, [product, selectedOptions, configurableOptions]);

  if (isLoading) {
    return (
      <RingSpinner />
    );
  }
  return (
    <>
      <div className={classes.headerInfo}>
        <div>
          <Typography color='primary' variant='body1'>
            Product Key: {product.productKey}
          </Typography>
          <Typography color='primary' variant='body1'>
            Version: {product.version}
          </Typography>
        </div>
        <FavoriteButton config={currentProductConfig} />
      </div>
      <ConfigurableOptionsList
        product={product}
        configurableOptions={configurableOptions}
        selectedOptions={selectedOptions}
        changeSelectedOption={changeSelectedOption}
      />
      <ButtonDock productConfig={currentProductConfig} />
    </>
  );
};
export default ConfigureProduct;
