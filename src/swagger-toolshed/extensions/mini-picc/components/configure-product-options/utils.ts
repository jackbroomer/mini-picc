import { ProductOptions } from '../../clients/productCatalogClient';
import { SelectedOption } from './types';

export const createSelectedConfigurableOptions = (
  configurableOptionsList: ProductOptions[],
  selectedOptions: SelectedOption[]
): Record<string, string> =>
  configurableOptionsList.reduce((acc, currOption, index) => {
    if (!selectedOptions[index]) {
      return acc;
    }
    acc[currOption.name] = selectedOptions[index] as string;
    return { ...acc };
  }, {} as Record<string, string>);
