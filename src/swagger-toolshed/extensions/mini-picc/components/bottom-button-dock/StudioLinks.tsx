import React, { useState, useRef, Ref } from 'react';
import RigidButton from '../../../../components/RigidButton';
import StudioLinkPopper from './StudioLinkPopper';
import { Configuration } from "../../../../types/Configuration";

type StudioLinksProps = {
  productConfig: Configuration;
}

const StudioLinks: React.FC<StudioLinksProps> = ({ productConfig }) => {
  const [isOpen, setIsOpen] = useState(false);
  const buttonRef = useRef<HTMLElement>();

  return (
    <>
      <RigidButton ref={buttonRef as Ref<HTMLElement> | undefined} onClick={() => setIsOpen((open) => !open)}>
        Studio Links
      </RigidButton>
      <StudioLinkPopper
        ref={buttonRef as Ref<HTMLElement> | undefined}
        isOpen={isOpen}
        close={() => setIsOpen(false)}
        productConfig={productConfig}
      />
    </>
  );
};
export default StudioLinks;