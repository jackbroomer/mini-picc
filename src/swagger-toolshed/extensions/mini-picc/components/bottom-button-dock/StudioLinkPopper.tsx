import React, {
  useState,
  useCallback,
  useMemo,
  forwardRef,
  useEffect,
  RefObject,
} from 'react';
import {
  ClickAwayListener,
  Popper,
  TextField,
  MenuItem,
  Paper,
  IconButton
} from '@material-ui/core';
import { OpenInNew } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import qs from 'qs';
import { validateStudioUrl } from '../../clients/studioLocaleClient';
import { Configuration } from '../../../../types/Configuration';

const useStyles = makeStyles((theme) => ({
  popper: {
    width: '100vw',
  },
  paper: {
    display: 'flex',
  },
  localeSuccess: {
    '& .MuiFormLabel-root': {
      color: theme.palette.success.main,
    },
    '& .MuiFilledInput-underline:before': {
      borderColor: theme.palette.success.main,
    },
  },
  linkIconButton: {
    margin: 'auto',
  },
}));

export const localeList = [
  'AT',
  'AU',
  'BE',
  'BR',
  'CA',
  'CH',
  'DE',
  'DK',
  'ES',
  'FI',
  'FR',
  'GB',
  'IE',
  'IN',
  'IT',
  'JP',
  'NL',
  'NO',
  'NZ',
  'PT',
  'SE',
  'SG',
  'US',
];

type StudioLinkPopperProps = {
  isOpen: boolean;
  productConfig: Configuration;
  close: () => void;
  ref: HTMLElement;
};

const StudioLinkPopper: React.ForwardRefRenderFunction<
  HTMLElement,
  StudioLinkPopperProps
> = ({ isOpen, productConfig, close }, popperAnchorRef) => {
  const classes = useStyles();
  const [isValidating, setIsValidating] = useState<boolean>(false);
  const [currentLocale, setCurrentLocale] = useState<string>('');
  const [isValidLocale, setIsValidLocale] = useState<boolean>(false);
  const [studioUrl, setStudioUrl] = useState<string>('');

  const buildStudioUrl = useCallback(
    (locale) => {
      let url = 'https://www.vistaprint.';
      const nextUrl = 'https://next.vistaprint';
      switch (locale) {
        case 'gb':
          url = url + `co.uk`;
          break;
        case 'us':
          url = url + `com`;
          break;
        case 'nz':
          url = url + `co.${locale}`;
          break;
        case 'au':
          url = url + `com.${locale}`;
          break;
        case 'ca':
          url = url + `ca`;
          break;
        case 'jp':
        case 'in':
          url = `${nextUrl}.${locale}`;
          break;
        default:
          url = url + locale;
          break;
      }
      const queryParams = qs.stringify({
        key: productConfig.productKey,
        selectedOptions: JSON.stringify(productConfig.optionSelections),
      });
      return `${url}/studio/?${queryParams}`;
    },
    [productConfig]
  );

  const onLocaleSelection = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentLocale(e.target.value);
  };

  const localeOptions = useMemo(
    () =>
      localeList.map((locale) => (
        <MenuItem key={locale} value={locale}>
          {locale}
        </MenuItem>
      )),
    []
  );

  const openInStudio = () => window.open(studioUrl, '_newTab');

  useEffect(() => {
    const setNewStudioUrl = async (locale: string) => {
      setIsValidating(true);
      const { passed } = await validateStudioUrl(productConfig, locale);
      setIsValidLocale(passed);
      setCurrentLocale(locale);
      setStudioUrl(passed ? buildStudioUrl(locale.toLowerCase()) : '');
      setIsValidating(false);
    };
    if (currentLocale) {
      setNewStudioUrl(currentLocale);
    }
  }, [productConfig, currentLocale, buildStudioUrl]);

  const hasError = !isValidating && !!currentLocale && !isValidLocale;
  return (
    <Popper
      className={classes.popper}
      anchorEl={(popperAnchorRef as RefObject<HTMLElement>).current}
      open={isOpen}
      placement='top-start'
    >
      <Paper className={classes.paper}>
        <TextField
          select
          label='Please select a locale'
          color='primary'
          variant='filled'
          fullWidth
          value={currentLocale}
          onChange={onLocaleSelection}
          error={hasError}
          helperText={
            hasError && 'This product is not configured for this market'
          }
          className={isValidLocale ? classes.localeSuccess : undefined}
        >
          {localeOptions}
        </TextField>
        <IconButton
          className={classes.linkIconButton}
          onClick={openInStudio}
          disabled={isValidating || !currentLocale || !isValidLocale}
          color='primary'
        >
          <OpenInNew />
        </IconButton>
      </Paper>
    </Popper>
  );
};
export default forwardRef(StudioLinkPopper);
