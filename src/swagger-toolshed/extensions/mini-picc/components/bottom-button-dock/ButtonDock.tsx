import { ButtonGroup } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ProductSurfaces from "./ProductSurfaces";
import StudioLinks from "./StudioLinks";
import DragAndDrop from "./DragAndDrop";
import { Configuration } from "../../../../types/Configuration";

const useStyles = makeStyles({
  buttonGroup: {
    position: "fixed",
    bottom: 0,
    left: 0,
  },
});

type ButtonDockProps = {
  productConfig: Configuration;
}

const ButtonDock: React.FC<ButtonDockProps> = ({ productConfig }) => {
  const classes = useStyles();
  return (
    <ButtonGroup
      color="default"
      variant="contained"
      className={classes.buttonGroup}
      fullWidth
    >
      <StudioLinks productConfig={productConfig} />
      <DragAndDrop productConfig={productConfig} />
      <ProductSurfaces productConfig={productConfig} />
    </ButtonGroup>
  );
}
export default ButtonDock;