import React from 'react';
import RigidButton from '../../../../components/RigidButton';
import { addRecentConfiguration } from '../../../../../syncStorageController';
import { useLocalStorageContext } from '../../../../contexts/LocalStorageContext';
import { Configuration } from '../../../../types/Configuration';
import DragIcon from '../../assets/drag.svg';

type DragAndDropProps = {
  productConfig: Configuration;
};

const DragAndDrop: React.FC<DragAndDropProps> = ({ productConfig }) => {
  const { additionalOptions } = useLocalStorageContext();

  const setData = (e: DragEvent) => {
    e?.dataTransfer?.setData(
      'swaggerToolshed',
      JSON.stringify({ ...productConfig, ...additionalOptions })
    );

    addRecentConfiguration(productConfig);
  };

  return (
    <RigidButton color='primary' draggable onDragStart={setData}>
      <DragIcon height={40} width={40} fill='white' />
      Drag and Drop
    </RigidButton>
  );
};
export default DragAndDrop;
