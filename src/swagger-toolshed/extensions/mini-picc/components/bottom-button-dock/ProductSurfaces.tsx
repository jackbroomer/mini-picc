import React, { useState, useEffect } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import { OpenInNew, Close } from "@material-ui/icons";
import RigidButton from "../../../../components/RigidButton";
import {
  getProductSurface,
  getProductSurfaceSvg,
} from "../../clients/surfaceServiceClient";
import { Configuration } from "../../../../types/Configuration";

const useStyles = makeStyles({
  dialog: {
    minWidth: "250px",
  },
  dialogContent: {
    textAlign: "center",
  },
  fadedLoader: {
    height: "100px",
    width: "min-content",
    transform: "translate(55px)",
  },
  closeButton: {
    position: "absolute",
    top: "2px",
    right: "2px",
  },
  surfaceImage: {
    width: "100%"
  }
});

type ProductSurfacesProps = {
  productConfig: Configuration;
}

const ProductSurfaces: React.FC<ProductSurfacesProps> = ({ productConfig }) => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);
  const [productSurfaceUrl, setProductSurfaceUrl] = useState("");
  const [productSurfaceSvgUrl, setProductSurfaceSvgUrl] = useState("");
  useEffect(() => {
    const surfaceUrl = getProductSurface(productConfig);
    const surfaceSvgUrl = getProductSurfaceSvg(productConfig);
    setProductSurfaceUrl(surfaceUrl);
    setProductSurfaceSvgUrl(surfaceSvgUrl);
  }, [productConfig]);

  const openInNewTab = () => window.open(productSurfaceUrl, "_newTab");

  return (
    <>
      <RigidButton onClick={() => setIsOpen(!isOpen)}>
        Product Surfaces
      </RigidButton>
      <Dialog open={isOpen} classes={{ paper: classes.dialog }}>
        <DialogTitle>
          Product Surfaces{" "}
          <IconButton
            className={classes.closeButton}
            onClick={() => setIsOpen(false)}
          >
            <Close />
          </IconButton>
        </DialogTitle>
        <DialogContent className={classes.dialogContent}>
          <img className={classes.surfaceImage} src={productSurfaceSvgUrl} alt="Product Surface Preview" />
        </DialogContent>
        <DialogActions>
          <Button onClick={openInNewTab} variant="contained" color="primary">
            <OpenInNew />
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
export default ProductSurfaces;
