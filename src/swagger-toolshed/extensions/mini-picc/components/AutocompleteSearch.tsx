import React, { useEffect, useState } from 'react';
import { Autocomplete } from '@material-ui/lab';
import {
  CircularProgress,
  IconButton,
  TextField,
  makeStyles,
} from '@material-ui/core';
import { ArrowBackIos } from '@material-ui/icons';
import {
  getAllProducts,
  ProductCatalogConfig,
} from '../clients/productCatalogClient';
import { SelectedProductConfiguration } from '../../../types/Configuration';

const useStyles = makeStyles({
  container: {
    width: '400px',
    height: '750px',
    position: 'relative',
    marginTop: '40px',
  },
  autocompleteContainer: {
    display: 'flex',
  },
  autocomplete: {
    width: '300px',
    margin: 'auto',
    padding: '15px',
  },
  appBar: {
    textAlign: 'center',
  },
  backButton: {
    position: 'absolute',
    margin: '20px 5px',
  },
  settingsButton: {
    position: 'fixed',
    bottom: '10px',
    right: '10px',
  },
});

type AutocompleteSearchProps = {
  selectedProduct: SelectedProductConfiguration | null;
  setSelectedProduct: (
    selectedProduct: SelectedProductConfiguration | null
  ) => void;
};

const AutocompleteSearch: React.FC<AutocompleteSearchProps> = ({
  selectedProduct,
  setSelectedProduct,
}) => {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [allProducts, setAllProducts] = useState<ProductCatalogConfig[]>([]);
  const [autocompleteValue, setAutocompleteValue] = useState<any>(null);

  // Retrieve all products
  useEffect(() => {
    const allProducts = getAllProducts();
    allProducts.then((response) => {
      setAllProducts(
        response.filter(({ productKey }) => productKey.startsWith('PRD'))
      );
      setIsLoading(false);
    });
  }, []);

  const onAutocompleteChange = (
    _event: React.ChangeEvent<unknown>,
    value: ProductCatalogConfig
  ) => {
    const product: SelectedProductConfiguration = {
      ...value,
      productKey: value.productKey,
    };

    setSelectedProduct(product);
    setAutocompleteValue(product);
  };

  const clearSearch = () => {
    setSelectedProduct(null);
    setAutocompleteValue(null);
  };

  return (
    <div className={classes.autocompleteContainer}>
      {selectedProduct && (
        <IconButton
          onClick={clearSearch}
          className={classes.backButton}
          color='primary'
        >
          <ArrowBackIos />
        </IconButton>
      )}
      <Autocomplete
        className={classes.autocomplete}
        options={allProducts}
        getOptionLabel={(option) => `${option.name} (${option.productKey})`}
        style={{ width: 300 }}
        value={autocompleteValue}
        renderInput={(params) => (
          <TextField
            autoFocus
            {...params}
            label={
              isLoading ? 'Gathering all products...' : 'Search for a Product'
            }
            variant='outlined'
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {isLoading ? (
                    <CircularProgress color='inherit' size={20} />
                  ) : null}
                  {params.InputProps.endAdornment}
                </>
              ),
            }}
          />
        )}
        disabled={isLoading}
        onChange={onAutocompleteChange}
        loading={isLoading}
      />
    </div>
  );
};
export default AutocompleteSearch;
