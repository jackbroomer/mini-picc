import React from 'react';
import ReactDOM from 'react-dom';
import App from './swagger-toolshed/components/App';
import LocalStorageProvider from './swagger-toolshed/contexts/LocalStorageContext';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#065c91',
    },
    secondary: {
      main: '#68c9ec',
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <LocalStorageProvider>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </LocalStorageProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
