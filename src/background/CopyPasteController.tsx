import ReactDOM from 'react-dom';
import { autoPopulateData, getPopulatedData } from './utils';
import { CopyPaste } from './components/CopyPaste';

const OP_BLOCK_SECTION_HEADER = 'opblock-section-header';

/**
 * Map through the target swagger request block and retrieve the populated parameters
 * and store the stringified result in the user's Clipboard.
 *
 * It get's stored with and "isSwaggerCopyParams" to indicate where the clipboard contents
 * came from.
 */
const copyRequestParamsToClipboard = async (id: string) => {
  const blockElement = document.getElementById(id) as HTMLElement;
  const type = 'text/plain';
  const blob = new Blob(
    [
      JSON.stringify({
        isSwaggerCopyParams: true,
        ...getPopulatedData(blockElement),
      }),
    ],
    {
      type,
    }
  );
  const data = [new ClipboardItem({ [type]: blob })];
  await navigator.clipboard.write(data);
};

/**
 * Uses the user's Clipboard contents to populate a swagger request block if the contents
 * contain the "isSwaggerCopyParams" attribute.
 */
const pasteRequestParamsToSwagger = async (id: string) => {
  const blockElement = document.getElementById(id) as HTMLElement;
  const clipboardText = JSON.parse(await navigator.clipboard.readText());

  if (clipboardText.isSwaggerCopyParams) {
    autoPopulateData(blockElement, clipboardText);
  } else {
    console.error('Clipboard data does not contain swagger request params');
  }
};

/**
 * A class responsible for appending the CopyPaste button components to the Swagger request blocks
 * as they appear. This is achieved by appending an empty <div /> element and using a Portal to
 * render the component as a child element on the page.
 */
class CopyPasteController {
  listenerIdMap: Record<string, string>;

  constructor() {
    this.listenerIdMap = {};
  }

  removeListerBlocks = (opBlocks: NodeListOf<Element>) => {
    const updatedListenerMap: Record<string, string> = {};

    [...opBlocks].forEach((opBlock) => {
      updatedListenerMap[opBlock.id] = this.listenerIdMap[opBlock.id];
    });

    this.listenerIdMap = updatedListenerMap;
  };

  addElementToOpBlocks = (opBlocks: NodeListOf<Element>) => {
    [...opBlocks] // Convert to iterable array
      .filter((opBlock) => !this.listenerIdMap[opBlock.id])
      .forEach((opBlock) => {
        // Retrieve the block header element (there's only one)
        const blockSectionHeader = opBlock.getElementsByClassName(
          OP_BLOCK_SECTION_HEADER
        )[0];

        if (blockSectionHeader) {
          this.listenerIdMap[opBlock.id] = opBlock.id;
          const copyPasteContainer = document.createElement('div');
          blockSectionHeader.append(copyPasteContainer);

          ReactDOM.render(
            <CopyPaste
              parentNode={blockSectionHeader as HTMLElement}
              onCopy={() => copyRequestParamsToClipboard(opBlock.id)}
              onPaste={() => pasteRequestParamsToSwagger(opBlock.id)}
            />,
            copyPasteContainer
          );
        }
      });
  };
}

export default new CopyPasteController();
