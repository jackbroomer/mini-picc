import React from 'react';
import { MapOfInputs, ReactInputElement } from './types';

// Map of elements with synonymous input labels
// TODO: The generic drag and drop controller should be unconcerned with this
const inputParamsToDataMap: Record<string, string> = {
  selectedOptions: 'optionSelections',
  version: 'productVersion',
};

// Function to match an input label to an existing synonmous input
const matchDataKeyToInputParam = (key: string): string => {
  const mappedKey: string = inputParamsToDataMap[key];
  return mappedKey || key;
};

// Take the list of params and values and map them to the existing swagger inputs
const mapDataParamsToInputElements = (block: HTMLElement): MapOfInputs => {
  // Get only data param fields from table
  const dataInputs = [...block.querySelectorAll('tr')].filter((tr) =>
    tr.hasAttribute('data-param-name')
  );

  // Get the inputs we need
  return dataInputs.reduce((acc, currInput) => {
    const dataParam = currInput.getAttribute('data-param-name') as string;
    const input:
      | HTMLInputElement
      | HTMLTextAreaElement
      | HTMLSelectElement
      | null =
      currInput.querySelector('input') ||
      currInput.querySelector('textarea') ||
      currInput.querySelector('select');

    return input ? { ...acc, [dataParam]: input } : acc;
  }, {});
};

/**
 * Find the reference to the react element we want and return the specified prop
 */
const getReactElementProp = <T>(
  input: ReactInputElement,
  targetProp: string
): T | undefined => {
  // A priority based list of keys/paths to check for the target react prop
  const reactKeyIdentifierPaths = [
    ['reactProps'], // 1st
    ['reactEvent'], // 2nd
    ['reactInternal', '_currentElement', 'props'], // Last
  ];

  let targetPropValue: T | undefined = undefined;

  reactKeyIdentifierPaths.find((identifierPath) => {
    // Traverse the root/current element using the identifier path/keys
    const targetElement = identifierPath.reduce(
      (acc: any, keyIdentifier: string) => {
        const targetKey = Object.keys(acc || input).find((key) =>
          key.includes(keyIdentifier)
        );
        return (acc || input)[targetKey as string];
      },
      undefined
    );

    if (targetElement) {
      targetPropValue = targetElement[targetProp];
      return true;
    }

    return false;
  });

  return targetPropValue;
};

// Extract the existing set of swagger inputs from the HTML block and map the data to the input fields
export const autoPopulateData = (
  block: HTMLElement,
  data: Record<string, string>
): void => {
  const mappedInputs: MapOfInputs = mapDataParamsToInputElements(block);
  Object.keys(mappedInputs).forEach((key) => {
    const mappedKey = matchDataKeyToInputParam(key);
    const targetData = data[mappedKey] ?? data[key];

    if (targetData) {
      const currentInput = mappedInputs[key] as unknown as ReactInputElement;
      const value =
        typeof targetData === 'object'
          ? JSON.stringify(targetData)
          : targetData;

      /**
       * This is a necessary hack for swagger to trigger the onChange event chain and
       * actually set the values. Soft setting the values with the HTMLElement properties
       * doesn't trigger any events and thus some internal logic does not fire.
       */
      const onChange = getReactElementProp<
        React.ChangeEventHandler<HTMLInputElement>
      >(currentInput, 'onChange');

      if (onChange) {
        onChange({
          persist: () => {},
          // @ts-ignore We don't need the other values
          target: { value, options: [] },
        });
      }
    }
  });
};

export const getPopulatedData = (
  block: HTMLElement
): Record<string, string> => {
  const data: Record<string, string> = {};
  const mappedInputs: MapOfInputs = mapDataParamsToInputElements(block);
  Object.keys(mappedInputs).forEach((key) => {
    const currentInput = mappedInputs[key] as unknown as ReactInputElement;
    const currentInputValue = getReactElementProp<string>(
      currentInput,
      'value'
    );

    if (currentInputValue) {
      // Set value
      data[key] = currentInputValue;
    }
  });

  return data;
};
