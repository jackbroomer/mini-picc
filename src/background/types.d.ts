export type MapOfInputs = Record<
  string,
  HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
>;

export type ReactInputElement = Record<
  string,
  {
    _currentElement: any;
  }
>;