import React, { useMemo, useState } from 'react';
import ReactDOM from 'react-dom';
import { IconButton, IconButtonProps, makeStyles, SvgIconTypeMap } from '@material-ui/core';
import { FileCopyRounded, Assignment } from '@material-ui/icons';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';

const useStyles = makeStyles({
  button: {
    margin: '0px 1px !important',
    borderRadius: '3px',
  },
  success: {
    color: '#49cc90',
  },
  error: {
    color: 'rgba(255, 0, 0, 0.6)',
  },
});

type StyledButtonProps = {
  Icon: OverridableComponent<SvgIconTypeMap<{}, 'svg'>>;
  onClick: () => Promise<void>;
} & IconButtonProps;

export const StyledButton = ({ Icon, onClick, ...rest }: StyledButtonProps) => {
  const classes = useStyles();
  const [actionSuccess, setActionSuccess] = useState(0);

  const handleClick = () => {
    onClick()
      .then(() => {
        setActionSuccess(1);
      })
      .catch(() => {
        setActionSuccess(-1);
      })
      .finally(() => {
        setTimeout(() => {
          setActionSuccess(0);
        }, 1000);
      });
  };

  const classname = useMemo(() => {
    let baseClass = classes.button;

    if (actionSuccess > 0) {
      baseClass += ` ${classes.success}`
    } else if (actionSuccess < 0) {
      baseClass += ` ${classes.error}`
    }

    return baseClass;
  }, [actionSuccess])

  return (
    <IconButton className={classname} onClick={handleClick} size='small' {...rest}>
      <Icon />
    </IconButton>
  );
};

type CopyPasteProps = {
  parentNode: HTMLElement;
  onCopy: () => Promise<void>;
  onPaste: () => Promise<void>;
};

export const CopyPaste = ({ parentNode, onCopy, onPaste }: CopyPasteProps) => {
  return ReactDOM.createPortal(
    <div style={{ margin: '0px 4px' }}>
      <StyledButton onClick={onCopy} Icon={FileCopyRounded} title="Copy" />
      <StyledButton onClick={onPaste} Icon={Assignment} title="Paste" />
    </div>,
    parentNode
  );
};
