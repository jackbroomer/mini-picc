import dragAndDropController from './DragAndDropController';
import copyPasteController from './CopyPasteController';

// TODO: Use options to enable/disable certain features
const observer = new MutationObserver((mutations) => {
  mutations.forEach((mutation) => {
    // Initialize listeners for any new codeblocks that appear that don't have already have them
    mutation.addedNodes.forEach(() => {
      const opBlocks = document.querySelectorAll('.opblock.is-open');
      if (opBlocks && opBlocks.length) {
        dragAndDropController.addDropListenersToOpBlocks(
          opBlocks as unknown as NodeListOf<HTMLElement>
        );
        copyPasteController.addElementToOpBlocks(opBlocks);
      }
    });
    mutation.removedNodes.forEach(() => {
      const opBlocks = document.querySelectorAll('.opblock.is-open');
      copyPasteController.removeListerBlocks(opBlocks);
    });
  });
});

observer.observe(document.body, {
  childList: true,
  subtree: true,
  attributes: false,
  characterData: false,
});
