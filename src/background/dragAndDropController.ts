import { autoPopulateData } from './utils';

const DRAG_AND_DROP_INDICATOR_ID = 'drag-and-drop-indicator';

const createDropIndicator = (height: number, width: number) => {
  const dropIndicator = document.createElement('div');
  dropIndicator.id = DRAG_AND_DROP_INDICATOR_ID;
  dropIndicator.style.position = 'absolute';
  dropIndicator.style.height = `${height}px`;
  dropIndicator.style.width = `${width}px`;
  dropIndicator.style.backgroundColor = 'black';
  dropIndicator.style.opacity = '0.5';
  return dropIndicator;
};

const attachDropIndicator = (dropTarget: HTMLElement) => {
  const dropIndicator = createDropIndicator(
    dropTarget.clientHeight,
    dropTarget.clientWidth
  );
  dropTarget.prepend(dropIndicator);
};

/**
 * A class responsible for appending/removing an opaque div over the Swagger request block to indicate
 * that a drag/drop action is active.
 */
class DragAndDropController {
  counter: number;
  listenerIdMap: Record<string, string>;

  constructor() {
    this.counter = 0;
    this.listenerIdMap = {};
  }

  incrementDropCounter() {
    this.counter += 1;
  }

  decrementDropCounter() {
    this.counter -= 1;
  }

  resetDropCounter() {
    this.counter = 0;
  }

  removeDropIndicator() {
    document.getElementById(DRAG_AND_DROP_INDICATOR_ID)?.remove();
    this.resetDropCounter();
  }

  onDragOver(e: DragEvent) {
    e.preventDefault();
  }

  // Increment the drag counter and attach the drop indicator if it isn't already
  onDragEnter(e: DragEvent) {
    this.incrementDropCounter();
    if (!document.getElementById(DRAG_AND_DROP_INDICATOR_ID)) {
      attachDropIndicator(e.currentTarget as HTMLElement);
    }
  }

  // Use counter to keep track of drag enter/leaves and determine when to remove indicator
  onDragLeave() {
    this.decrementDropCounter();
    if (this.counter === 0) {
      this.removeDropIndicator();
    }
  }

  onDrop(event: DragEvent, blockId: string) {
    // Data is sent as a string so parse
    try {
      const data = JSON.parse(
        event?.dataTransfer?.getData('swaggerToolshed') as string
      );
      const targetBlock = document.querySelector(`#${blockId}`) as HTMLElement;

      if (targetBlock) {
        autoPopulateData(targetBlock, data);
      }
    } catch (error) {
      console.error('Cannot auto-populate data fields', error);
      console.error('DATA:', event?.dataTransfer?.getData('swaggerToolshed') || null);
    } finally {
      // Remove drag indicator and reset counter
      this.removeDropIndicator();
    }
  }

  // Attach drag listeners to the swagger request blocks so they can respond to drag events
  addDropListenersToOpBlocks(opBlocks: NodeListOf<HTMLElement>) {
    [...opBlocks]
      .filter((opBlock) => !this.listenerIdMap[opBlock.id])
      .forEach((opBlock) => {
        this.listenerIdMap[opBlock.id] = opBlock.id;
        opBlock.addEventListener('dragover', this.onDragOver, false);
        opBlock.addEventListener(
          'dragenter',
          this.onDragEnter.bind(this),
          false
        );
        opBlock.addEventListener(
          'dragleave',
          this.onDragLeave.bind(this),
          false
        );
        opBlock.addEventListener(
          'drop',
          function (this: any, e: DragEvent) {
            this.onDrop(e, opBlock.id);
          }.bind(this)
        );
      });
  };
}

export default new DragAndDropController();
