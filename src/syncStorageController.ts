import { ProductCatalogConfig } from './swagger-toolshed/extensions/mini-picc/clients/productCatalogClient';
import { Configuration } from './swagger-toolshed/types/Configuration';
import { AppState } from './swagger-toolshed/types/Settings';

export enum StorageKeys {
  MINI_PICC_RECENTS = 'MINI_PICC_RECENTS',
  MINI_PICC_FAVORITES = 'MINI_PICC_FAVORITES',
  MINI_PICC_ADDITIONAL_OPTIONS = 'MINI_PICC_ADDITIONAL_OPTIONS',
  MINI_PICC_APP_STATE = 'MINI_PICC_APP_STATE',
  MINI_PICC_PRODUCT_CATALOG = 'MINI_PICC_PRODUCT_CATALOG',
}

type Recents = {
  MINI_PICC_RECENTS: Configuration[];
};

type Favorites = {
  MINI_PICC_FAVORITES: Configuration[];
};

type AdditionalOptions = {
  MINI_PICC_ADDITIONAL_OPTIONS: Record<string, string>;
};

type ApplicationState = {
  MINI_PICC_APP_STATE: AppState;
};

type ProductCatalogCache = {
  MINI_PICC_PRODUCT_CATALOG: {
    productCatalog: ProductCatalogConfig[];
    ttl: number;
  };
};

// RECENT CONFIGS
export const getRecentProducts = (
  callback: (data: Configuration[]) => void
): void => {
  chrome.storage.local.get(StorageKeys.MINI_PICC_RECENTS, (data: Recents) => {
    callback(data.MINI_PICC_RECENTS);
  });
};

export const addRecentConfiguration = (productConfig: Configuration): void => {
  chrome.storage.local.get(StorageKeys.MINI_PICC_RECENTS, (data: Recents) => {
    const recents = data.MINI_PICC_RECENTS;
    chrome.storage.local.set(
      {
        [StorageKeys.MINI_PICC_RECENTS]: [productConfig, ...recents],
      },
      function () {
        console.log('Recent Products Updated');
      }
    );
  });
};

export const removeRecentProduct = (productConfigId: string): void => {
  chrome.storage.local.get(StorageKeys.MINI_PICC_RECENTS, (data: Recents) => {
    const recents = data.MINI_PICC_RECENTS;

    const updatedRecentProducts = recents.filter(
      ({ config_uuid }) => config_uuid !== productConfigId
    );
    chrome.storage.local.set(
      { [StorageKeys.MINI_PICC_RECENTS]: [...updatedRecentProducts] },
      function () {
        console.log('Recent Products Updated');
      }
    );
  });
};

// FAVORITE CONFIGS
export const getFavoriteProducts = (
  callback: (data: Configuration[]) => void
): void => {
  chrome.storage.local.get(
    StorageKeys.MINI_PICC_FAVORITES,
    (data: Favorites) => {
      callback(data.MINI_PICC_FAVORITES);
    }
  );
};

export const addFavoriteProduct = (productConfig: Configuration): void => {
  chrome.storage.local.get(
    StorageKeys.MINI_PICC_FAVORITES,
    (data: Favorites) => {
      let didUpdate = false;

      // Update product if the UUID already exists
      const favorites = data.MINI_PICC_FAVORITES.map((favorite) => {
        if (favorite.config_uuid === productConfig.config_uuid) {
          didUpdate = true;
          return productConfig;
        }
        return favorite;
      });

      // If no update was made, this is a new favorite so add it
      if (!didUpdate) {
        favorites.push(productConfig);
      }

      chrome.storage.local.set(
        {
          [StorageKeys.MINI_PICC_FAVORITES]: favorites,
        },
        function () {
          console.log('Favorite Products Updated');
        }
      );
    }
  );
};

// Update this to remove by ID
export const removeFavoriteProduct = (productConfigId: string): void => {
  chrome.storage.local.get(
    StorageKeys.MINI_PICC_FAVORITES,
    (data: Favorites) => {
      const favorites = data.MINI_PICC_FAVORITES;
      // Remove current object
      const updatedFavoriteProducts = favorites.filter(
        ({ config_uuid }) => config_uuid !== productConfigId
      );
      chrome.storage.local.set(
        { [StorageKeys.MINI_PICC_FAVORITES]: [...updatedFavoriteProducts] },
        function () {
          console.log('Recent Products Updated');
        }
      );
    }
  );
};

// Additional Options
export const getAdditionalOptions = (
  callback: (data: Record<string, string>) => void
): void => {
  chrome.storage.local.get(
    StorageKeys.MINI_PICC_ADDITIONAL_OPTIONS,
    (data: AdditionalOptions) => {
      callback(data.MINI_PICC_ADDITIONAL_OPTIONS);
    }
  );
};

export const setAdditionalOptions = (
  settings: Record<string, string>
): void => {
  chrome.storage.local.set(
    { [StorageKeys.MINI_PICC_ADDITIONAL_OPTIONS]: settings },
    function () {
      console.log('Additional Settings Updated');
    }
  );
};

// App State
export const getAppState = (callback: (data: AppState) => void): void => {
  chrome.storage.local.get(
    StorageKeys.MINI_PICC_APP_STATE,
    (data: ApplicationState) => {
      callback(data.MINI_PICC_APP_STATE);
    }
  );
};

export const setAppState = (appState: AppState): void => {
  chrome.storage.local.set(
    { [StorageKeys.MINI_PICC_APP_STATE]: appState },
    () => {
      console.log('AppState Updated');
    }
  );
};

// Cache the whole product catalog in local storage
export const getProductCatalogCache = (
  callback: (data: ProductCatalogCache['MINI_PICC_PRODUCT_CATALOG']) => void
): void => {
  chrome.storage.local.get(
    StorageKeys.MINI_PICC_PRODUCT_CATALOG,
    (data: ProductCatalogCache) => {
      callback(data.MINI_PICC_PRODUCT_CATALOG);
    }
  );
};

export const setProductCatalogCache = (
  productCatalog: ProductCatalogConfig[]
): void => {
  chrome.storage.local.set(
    {
      [StorageKeys.MINI_PICC_PRODUCT_CATALOG]: {
        productCatalog,
        ttl: Date.now() + 86400 * 1000, // Expire in 1 day
      },
    },
    () => {
      console.log('Product Catalog Cached');
    }
  );
};

export const INITIAL_APP_STATE: AppState = {
  isFavoritesOpen: false,
  isRecentsOpen: false,
  lastSelectedProduct: null,
  lastSelectedOptions: null,
};

export const initializeLocalStorage = (): void => {
  getRecentProducts((recents) => {
    if (!recents || !recents.length) {
      chrome.storage.local.set({ [StorageKeys.MINI_PICC_RECENTS]: [] });
    }
  });
  getFavoriteProducts((favorites) => {
    if (!favorites || !favorites.length) {
      chrome.storage.local.set({ [StorageKeys.MINI_PICC_FAVORITES]: [] });
    }
  });
  getAdditionalOptions((additionalOptions) => {
    if (!additionalOptions) {
      setAdditionalOptions({});
    }
  });
  getAppState((appState) => {
    if (!appState) {
      setAppState(INITIAL_APP_STATE);
    }
  });
};
